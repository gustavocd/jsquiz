import Vue from "vue";
import Vuex from "vuex";
import questions from "@/data";

Vue.use(Vuex);

const SET_ANSWER = "SET_ANSWER";
const SHOW_EXPLANATION = "SHOW_EXPLANATION";

export default new Vuex.Store({
  state: {
    questions,
    answer: null,
    currentIndex: 0,
    showConfetti: false,
    incorrectQuestions: 0,
    correctQuestions: 0,
    confettiTimeout: 4000,
    letters: ["A", "B", "C", "D", "E", "F"]
  },
  mutations: {
    [SET_ANSWER](state, payload) {
      state.answer = payload;
    },
    [SHOW_EXPLANATION](state) {
      if (state.answer.correct) {
        state.correctQuestions++;
      } else {
        state.incorrectQuestions++;
      }
      state.currentIndex++;
      state.answer = null;
    },
    SHOW_CONFETTI(state) {
      state.showConfetti = !state.showConfetti;
    }
  },
  actions: {
    onSelectAnswer({ commit }, payload) {
      commit("SET_ANSWER", payload);
    },
    onCheckAnswer({ state, commit }) {
      if (state.answer.correct) {
        commit("SHOW_CONFETTI");
        setTimeout(() => {
          commit("SHOW_CONFETTI");
        }, state.confettiTimeout);
      }
    },
    onShowExplanation({ commit }) {
      commit("SHOW_EXPLANATION");
    }
  },
  getters: {
    currentQuestion: ({ currentIndex, questions }) =>
      questions.slice(currentIndex, currentIndex + 1),
    questions: ({ questions }) => questions,
    answer: ({ answer }) => answer,
    letters: ({ letters }) => letters,
    incorrectQuestions: ({ incorrectQuestions }) => incorrectQuestions,
    correctQuestions: ({ correctQuestions }) => correctQuestions,
    showConfetti: ({ showConfetti }) => showConfetti,
    confettiTimeout: ({ confettiTimeout }) => confettiTimeout
  }
});
